class CourseUsersController < ApplicationController
    def create
        puts params[:course_id]
        course = Course.find(params[:course_id])
        puts course
        current_user.course_users.build(course_id: course.id)
        if current_user.save
            flash[:notice] = "Successfully enrolled to the course"
        else
            flash[:alert] = "Something went wrong"
        end
        
        redirect_to courses_path
        
    end
end
class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :course_users
  has_many :courses, through: :course_users
  
  def is_enrolled?(course)
    self.courses.where(id: course).exists?
  end
end

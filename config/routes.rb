Rails.application.routes.draw do
  resources :courses
  devise_for :users
  root 'welcome#index'
  resources :students, only: [:index, :show]
  post 'enroll', to: 'course_users#create'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
